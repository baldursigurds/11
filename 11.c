#include <stdlib.h>
#include <ncurses.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

// Height and width
#define WT 4
#define HT 4

void menu();
int  add(int ** b);
void draw_board(int ** b);
int push_board(int ** b, int dir, int * score);
int pic(int s);
int game_over();
int play();
int dead(int ** b);

int clear_board();
int high_score(int score, int ** b);

int sd();
int sd_finished(char ** mf, char ** bombs, int spots, int sdHT, int sdWT);
int sd_print(char ** mf, int sdHT, int sdWT);
void sd_push(char ** tree, char ** bombs, char ** mf, int I, int J, int sdHT, int sdWT);
char ** sd_drop_bombs(int sdHT, int sdWT, int n);
void sd_win(int t);
void sd_lose(char ** mf, int sdHT, int sdWT);

void sudoku();
char ** init_sud_board(int diff);
int sud_draw(char ** sud_board, int I, int J, char c);
int sud_check(char ** sud_board, int i, int j, char c);

typedef struct card card;
typedef struct sol_game sol_game;
void solitaire();
card * init_deck();
void print_card(int i, int j, card * C, int hidden);
int deck_length(card * C);
void shuffle_deck(card ** CC);
void push_card(card * C, card ** DD);
card * pop_card(card ** DD);
void print_sol_game(sol_game * game);
void draw_card(sol_game * game);
int sol_game_won(sol_game * game);
void flip_deck(card ** DD);
void move_cards(sol_game * game, int I, int J);
void take_card(sol_game * game, int I, int J);
void base_change(sol_game * game, int I, int J, int b);
int card_fits(sol_game * game, card * C, int I, int J);
void flip_card(sol_game * game, int J);
void take_to_base(sol_game * game);
void win_sol_game();
void sol_show_help();
void free_sol_game(sol_game * game);
void free_deck(card * D);

typedef struct bet_piece bet_piece;
void betris();
void betris_print(char ** B, bet_piece piece);
int betris_fits(char ** bet_board, bet_piece piece, char **** bet_model);
char **** bet_init_models();
void bet_print_piece(bet_piece piece, char **** models, int next);
void bet_write_piece(char ** bet_board, bet_piece piece, char **** models);
int betris_rollup(char ** bet_board);


struct hse
{
    int score;
    int b[HT*WT];
    char name[4];
};

struct card
{
    // 1 = ace, 11 = jack, etc.
    int n;
    // 0 = hearts, 1 = clubs, 2 = diamonds, 3 = spades.
    int s;
    card * next;
    card * prev;
};

struct sol_game
{
    // 7 columns, entry 0 is the stack, others are the list coming down
    card * col[14][7];
    // the closed stack from which to draw
    card * stackc;
    // the open stack, from which to put onto the table
    card * stacko;
    // the base, fill it up to win!
    card * base[4];
    // the card selected for movement, or {-1,0}
    int selected[2];
};

struct bet_piece
{
    int type;
    int direction;
    int x;
    int y;
};




int main()
{
    // Initiate some variables and random and ncurses stuff.
    srand(time(NULL));
    initscr();
    clear();
    noecho();
    cbreak();

    start_color();
    init_color(20, 300, 500, 0);
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_YELLOW, COLOR_BLACK);
    init_pair(4, COLOR_BLUE, COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6, COLOR_CYAN, COLOR_BLACK);
    init_pair(7, COLOR_RED, COLOR_YELLOW);
    init_pair(8, COLOR_GREEN, COLOR_YELLOW);
    init_pair(9, COLOR_BLUE, COLOR_YELLOW);
    init_pair(10, COLOR_MAGENTA, COLOR_YELLOW);
    init_pair(11, COLOR_CYAN, COLOR_YELLOW);
    init_pair(12, COLOR_RED, COLOR_CYAN);
    init_pair(13, COLOR_GREEN, COLOR_CYAN);
    init_pair(14, COLOR_YELLOW, COLOR_CYAN);
    init_pair(15, COLOR_BLUE, COLOR_CYAN);

    init_pair(20, COLOR_WHITE, COLOR_BLACK);
    init_pair(21, COLOR_BLACK, COLOR_WHITE);

    // Sudden death:
    // unknown:
    init_pair(30, COLOR_WHITE, COLOR_GREEN);
    // flagged:
    init_pair(31, COLOR_WHITE, COLOR_RED);
    // known:
    init_pair(32, COLOR_YELLOW, COLOR_BLACK);
    // yellow part of the fire!
    init_pair(33, COLOR_WHITE, COLOR_YELLOW);

    // Sudoku:
    // board:
    init_pair(41, COLOR_BLACK, COLOR_GREEN);
    // error:
    init_pair(42, COLOR_BLACK, COLOR_RED);
    // frame 1:
    init_pair(43, COLOR_BLUE, COLOR_BLUE);
    // frame 1:
    init_pair(44, COLOR_YELLOW, COLOR_YELLOW);

    // Cards:
    // Card backface
    init_pair(50, COLOR_WHITE, COLOR_BLUE);
    // Red cards
    init_pair(51, COLOR_RED, COLOR_WHITE);
    // Black cards
    init_pair(52, COLOR_BLACK, COLOR_WHITE);
    // Tabletop
    init_pair(53, COLOR_BLACK, COLOR_GREEN);

    // betris:
    init_pair(60, COLOR_WHITE, COLOR_BLACK);
    init_pair(61, COLOR_BLACK, COLOR_RED);
    init_pair(62, COLOR_BLACK, COLOR_GREEN);
    init_pair(63, COLOR_BLACK, COLOR_YELLOW);
    init_pair(64, COLOR_BLACK, COLOR_BLUE);
    init_pair(65, COLOR_BLACK, COLOR_MAGENTA);
    init_pair(66, COLOR_BLACK, COLOR_CYAN);
    init_pair(67, COLOR_BLACK, COLOR_WHITE);

    // help menus
    init_pair(90, COLOR_YELLOW, COLOR_BLACK);
    // The menu
    init_pair(91, COLOR_YELLOW, COLOR_BLACK);


    menu();


    endwin();

    return 0;
}

void menu()
{
    char c;
    int i,j;
    // q is the quit switch
    int q = 0;
    int state = 0;
    char *m_items[] =
    {
        "11           ", // 0
        "Sudden death!", // 1
//        "Sudoku       ",
        "Solitaire    ", // 2
        "betris       ", // 3
        "High scores  ", // 4
        "Quit         ", // 5
        "             ",
        (char *)NULL,
    };
    int m_len = 6;
    while(!q)
    {
        pic(rand()%7);
        for(i=0; i<m_len; i++)
        {
            if(i==state)
                mvprintw(5+2*i, 30, "-> ");
            else
                mvprintw(5+2*i, 30, "   ");
            mvprintw(5+2*i, 33, m_items[i]);
        }
        refresh();
        c = getch();
        switch(c)
        {
            case 'k': // Up!
                state--;
                if(state == -1)
                    state = m_len - 1;
                break;

            case 'j': // Down!
                state++;
                if(state == m_len)
                    state = 0;
                break;

            case '\033':
                getch();
                switch(getch())
                {
                    case 'A': // Up!
                        state--;
                        if(state == -1)
                            state = m_len - 1;
                        break;

                    case 'B': // Down!
                        state++;
                        if(state == m_len)
                            state = 0;
                        break;
                }
                break;
            case '\n':
                switch(state)
                {
                    case 0:
                        play();
                        break;
                    case 1:
                        sd(0, NULL);
                        break;
//                    case 2:
//                        sudoku();
//                        break;
                    case 2:
                        solitaire();
                        break;
                    case 3:
                        betris();
                        break;
                    case 4:
                        high_score(0, NULL);
                        break;
                    case 5:
                        q = 1;
                        break;
                }
        }
        
    }
}

int high_score(int score, int ** b)
{
    clear_board();
    pic(0);
    int i,j,k,rk;
    int ten;
    int state = 0;
    char * hs_file = "11.hs";
    FILE *LIST;
    struct hse li[10];
    char init;
    if(LIST = fopen(hs_file, "r"))
    {
        fread(li, sizeof(struct hse), 10, LIST);
        fclose(LIST);
    }
    else
    {
        for(k=0; k<10; k++)
        {
            li[k].score = 0;
            li[k].name[0] = 'X';
            li[k].name[1] = 'X';
            li[k].name[2] = 'X';
            li[k].name[3] = '\0';
            for(i=0; i<WT; i++)
            {
                for(j=0; j<HT; j++)
                    li[k].b[i*WT+j] = 0;
            }
        }
        LIST = fopen(hs_file, "w");
        fwrite(li, sizeof(struct hse), 10, LIST);
        fclose(LIST);
    }

    if(score != 0 && b != NULL)
    {
        rk = 0;
        while(score <= li[rk].score && rk<10)
            rk++;
        if(rk<10)
        {
            for(i=9; i<rk; i--)
            {
                li[i] = li[i+1];
            }

            struct hse new_hse;
            new_hse.score = score;
            for(i=0; i<HT; i++)
            {
                for(j=0; j<WT; j++)
                {
                    new_hse.b[i*WT+j] = b[i][j];
                }
            }
            mvprintw(5,33,"You made it to the");
            mvprintw(6,33,"highscore list!");
            mvprintw(7,33,"Write your initials:");
            for(i=0; i<3; i++)
            {
                init = '0';
                while(init == '0')
                {
                    init = getch();
                    if('A' <= init && init <= 'Z')
                    {
                        mvprintw(8,33+i,"%c", init);
                        new_hse.name[i] = init;
                    }
                    else
                    {
                        init = '0';
                        mvprintw(9,33,"A-Z only please!");
                    }
                }
                new_hse.name[3] = '\0';
                refresh();
            }
            clear_board();
            pic(0);

            li[rk] = new_hse;
        }
        LIST = fopen(hs_file, "w+");
        fwrite(li, sizeof(struct hse), 10, LIST);
        fclose(LIST);
    }
    
    int q = 0;
    char c;
    int ** B = malloc(WT*sizeof(int*));
    for(i=0;i<HT;i++)
        B[i] = malloc(HT*sizeof(int));


    while(!q)
    {
        clear_board();
        pic(0);
        for(i=0; i<10; i++)
        {
            ten = 0;
            j = 1;
            while(li[i].score >= j)
            {
                j = j*10;
                ten++;
            }
            if(ten==0)
                ten++;
            if(i==state)
                attron(COLOR_PAIR(21));
            else
                attron(COLOR_PAIR(20));
            mvprintw(5+i, 33, "%d %s ", i, li[i].name);
            for(j=0; j<8-ten; j++)
                mvprintw(5+i, 39+j, ".");
            mvprintw(5+i, 47-ten, " %d", li[i].score);
                
            if(i==state)
                attroff(COLOR_PAIR(21));
            else
                attroff(COLOR_PAIR(20));
        }

        if(state==10)
            attron(COLOR_PAIR(21));
        else
            attron(COLOR_PAIR(20));
            mvprintw(15, 33, "  Back        ");
        if(state==10)
            attroff(COLOR_PAIR(21));
        else
            attroff(COLOR_PAIR(20));
        refresh();
        c = getch();
        switch(c)
        {
            case 'k': // Up!
                state--;
                if(state == -1)
                    state = 10;
                break;

            case 'j': // Down!
                state++;
                if(state == 11)
                    state = 0;
                break;

            case '\033':
                getch();
                switch(getch())
                {
                    case 'A': // Up!
                        state--;
                        if(state == -1)
                            state = 10;
                        break;

                    case 'B': // Down!
                        state++;
                        if(state == 11)
                            state = 0;
                        break;
                }
                break;
            case '\n':
                if(state == 10)
                    q = 1;
                else
                {
                    clear_board();
                    pic(0);
                    for(i=0; i<HT; i++)
                        for(j=0; j<WT; j++)
                            B[i][j] = li[state].b[i*WT+j];
                    draw_board(B);
                    while(getch() != '\n');
                }
                break;

        }
        
    }
    clear_board();
}

int play()
{
    int go = 1;
    int i,j,r;
    int score = 0;
    char c;
    // Create the board. Usually, i goes from 0 to WT-1, j from 0 to HT-1.
    int ** b = malloc(WT*sizeof(int*));
    for(i=0;i<HT;i++)
        b[i] = malloc(HT*sizeof(int));

    for(i=0; i<HT; i++)
        for(j=0; j<WT; j++)
            b[i][j] = 0;


    clear_board();
    pic(1);

    add(b);
    add(b);

    while(!dead(b))
    {
        draw_board(b);
        mvprintw(14, 38, "%d", score);
        refresh();
        c = getch();
        switch(c)
        {
            case 'l':
                r = push_board(b,0,&score);
                break;

            case 'j':
                r = push_board(b,1,&score);
                break;

            case 'h':
                r = push_board(b,2,&score);
                break;

            case 'k':
                r = push_board(b,3,&score);
                break;
        }
        if(r)
            add(b);
    }
    draw_board(b);

    // Clean up.
    game_over();
    high_score(score, b);
    for(i=0;i<HT;i++)
        free(b[i]);
    free(b);
    return(score);
}

int dead(int ** b)
{
    int i,j;
    for(i=0; i<WT-1; i++)
        for(j=0; j<HT; j++)
            if(b[i][j] == b[i+1][j])
                return 0;
    for(i=0; i<WT; i++)
        for(j=0; j<HT-1; j++)
            if(b[i][j] == b[i][j+1])
                return 0;
    for(i=0; i<WT; i++)
        for(j=0; j<HT; j++)
            if(b[i][j] == 0)
                return 0;
    return 1;
}

int push_board(int ** b, int dir, int * score)
{
    int i,j,k,ma;
    int r = 0;
    int d[HT*WT];
    for(i=0;i<WT;i++)
        for(j=0;j<HT;j++)
            d[HT*i+j] = 0;
    ma = 0;
    switch(dir)
    {
        case 0:
            // this pushes to the right
            for(i=WT-2; i>=0; i--)
            for(j=0; j<HT; j++)
            {
                if(b[i][j] == 0)
                    continue;

                k = 0;
                while(i+k+1<WT && b[i+k+1][j] == 0)
                    k++;

                if(i+k+1 < WT && b[i][j] == b[i+k+1][j] && !d[HT*(i+k+1) + j])
                {
                    b[i][j] = 0;
                    b[i+k+1][j]++;
                    *score += (1 << b[i+k+1][j]);
                    if(b[i+k+1][j] > ma)
                        ma = b[i+k+1][j];
                    d[HT*(i+k+1) + j] = 1;
                    r = 1;
                }
                else if(k>0)
                {
                    b[i+k][j] = b[i][j];
                    b[i][j] = 0;
                    r = 1;
                }
            }
            break;

        case 1:
            // this pushes down
            for(j=HT-2; j>=0; j--)
            for(i=0; i<WT; i++)
            {
                if(b[i][j] == 0)
                    continue;

                k = 0;
                while(j+k+1<HT && b[i][j+k+1] == 0)
                    k++;

                if(j+k+1 < HT && b[i][j] == b[i][j+k+1] && !d[HT*i + j+k+1])
                {
                    b[i][j] = 0;
                    b[i][j+k+1]++;
                    *score += (1 << b[i][j+k+1]);
                    if(b[i][j+k+1] > ma)
                        ma = b[i][j+k+1];
                    d[HT*i + j+k+1] = 1;
                    r = 1;
                }
                else if(k>0)
                {
                    b[i][j+k] = b[i][j];
                    b[i][j] = 0;
                    r = 1;
                }
            }
            break;

        case 2:
            // this pushes to the left
            for(i=1; i<WT; i++)
            for(j=0; j<HT; j++)
            {
                if(b[i][j] == 0)
                    continue;

                k = 0;
                while(i-k-1>=0 && b[i-k-1][j] == 0)
                    k++;

                if(i-k-1 >= 0 && b[i][j] == b[i-k-1][j] && !d[(i-k-1)*HT+j])
                {
                    b[i][j] = 0;
                    b[i-k-1][j]++;
                    *score += (1 << b[i-k-1][j]);
                    if(b[i-k-1][j]>ma)
                        ma = b[i-k-1][j];
                    d[(i-k-1)*HT+j] = 1;
                    r = 1;
                }
                else if(k>0)
                {
                    b[i-k][j] = b[i][j];
                    b[i][j] = 0;
                    r = 1;
                }
            }
            break;

        case 3:
            // this pushes up
            for(j=1; j<HT; j++)
            for(i=0; i<WT; i++)
            {
                if(b[i][j] == 0)
                    continue;

                k = 0;
                while(j-k-1>=0 && b[i][j-k-1] == 0)
                    k++;

                if(j-k-1 >= 0 && b[i][j] == b[i][j-k-1] && !d[i*HT+j-k-1])
                {
                    b[i][j] = 0;
                    b[i][j-k-1]++;
                    *score += (1 << b[i][j-k-1]);
                    if(b[i][j-k-1] > ma);
                        ma = b[i][j-k-1];
                    d[i*HT+j-k-1] = 1;
                    r = 1;
                }
                else if(k>0)
                {
                    b[i][j-k] = b[i][j];
                    b[i][j] = 0;
                    r = 1;
                }
            }
            break;
    }
    pic(ma);
    return r;
}

void draw_board(int ** b)
{
    int i,j;
    for(i=0;i<WT;i++)
        for(j=0;j<HT;j++)
        {
            attron(COLOR_PAIR(b[i][j]));
            mvprintw(2*j+5,3*i+34,"%s%d",
                (b[i][j]<10?" ":""), b[i][j]%20);
            attroff(COLOR_PAIR(b[i][j]));
        }
}

int add(int ** b)
{
    int i,j,r;
    int * list = malloc(WT*HT*sizeof(int));
    int n = 0;
    for(i=0; i<WT; i++)
        for(j=0; j<HT; j++)
            if(b[i][j] == 0)
            {
                list[n] = HT*i+j;
                n++;
            }
    if(n==0)
    {
        free(list);
        return 0;
    }

    r = rand()%n;
    i = (list[r]/HT)%WT;
    j = list[r]%HT;
    b[i][j]++;

    if(!(rand()%5))
        b[i][j]++;
    free(list);
    return 1;
}

int pic(int s)
{
    attron(COLOR_PAIR(s));
    mvprintw( 5,10,"+--------------+");
    mvprintw( 6,10,"|  _|      _|  |");
    mvprintw( 7,10,"|_|_|    _|_|  |");
    mvprintw( 8,10,"|  _|      _|  |");
    mvprintw( 9,10,"|  _|      _|  |");
    mvprintw(10,10,"|  _|      _|  |");
    mvprintw(11,10,"|  _|      _|  |");
    mvprintw(12,10,"|_|_|_|  _|_|_||");
    mvprintw(13,10,"+--------------+");
    mvprintw( 5,54,"+--------------+");
    mvprintw( 6,54,"|  _|      _|  |");
    mvprintw( 7,54,"|_|_|    _|_|  |");
    mvprintw( 8,54,"|  _|      _|  |");
    mvprintw( 9,54,"|  _|      _|  |");
    mvprintw(10,54,"|  _|      _|  |");
    mvprintw(11,54,"|  _|      _|  |");
    mvprintw(12,54,"|_|_|_|  _|_|_||");
    mvprintw(13,54,"+--------------+");
    attroff(COLOR_PAIR(s));
}

int game_over()
{
//    int i;
//    for(i=0;i<100;i++)
//    {
//        mvprintw(rand()%24, rand()%70, "Game over!");
//        refresh();
//        nanosleep((struct timespec[]){{0, 50000000}}, NULL);
//    }
//    for(i=0; i<24; i++)
//    {
//        mvprintw(i, 0, "                                                                                ");
//    }
    mvprintw(15, 38, "Game over!");
    while(getch() != '\n');
    return 0;
}

int clear_board()
{
    int i;
    for(i=0; i<24; i++)
    {
        mvprintw(i, 0, "                                                                                ");
    }
}

int sd()
{
    int sdHT,sdWT;
    int i,j,b;
    int fin = 0;
    sdHT = 16;
    sdWT = 30;
    int spots = 99;
    int in_H = (24 - sdHT)/2;
    int in_W = (80 - sdWT)/2;
    int I = sdHT/2;
    int J = sdWT/2;
    char ** mf = (char **) malloc((sdHT)*sizeof(char *));
    char ** tree = (char **) malloc((sdHT)*sizeof(char *));
    char ** bombs = sd_drop_bombs(sdHT, sdWT, spots);
    for(i=0; i<sdHT; i++)
    {
        mf[i] = (char *) malloc((sdWT+1)*sizeof(char));
        tree[i] = (char *) malloc((sdWT+1)*sizeof(char));
        for(j=0; j<sdWT; j++)
        {
            mf[i][j] = '.';
            tree[i][j] = ' ';
        }
        mf[i][sdWT] = '\0';
    }

    int t = time(NULL);
    while(!fin)
    {
        sd_print(mf, sdHT, sdWT);
        move(I+in_H,J+in_W);
        switch(getch())
        {
            case 'h':
                if(J>0)
                    J--;
                break;
            case 'l':
                if(J<sdWT-1)
                    J++;
                break;
            case 'j':
                if(I<sdHT-1)
                    I++;
                break;
            case 'k':
                if(I>0)
                    I--;
                break;
            case 'y':
                if(I>0 && J>0)
                {
                    I--;
                    J--;
                }
                break;
            case 'u':
                if(I>0 && J<sdWT-1)
                {
                    I--;
                    J++;
                }
                break;
            case 'b':
                if(I<sdHT-1 && J>0)
                {
                    I++;
                    J--;
                }
                break;
            case 'n':
                if(I<sdHT-1 && J<sdWT-1)
                {
                    I++;
                    J++;
                }
                break;
            case 'f':
                if(mf[I][J] == '.')
                    mf[I][J] = 'F';
                else if(mf[I][J] == 'F')
                    mf[I][J] = '.';
                break;
            case '\n':
            case ' ':
                if(bombs[I][J] == 'X')
                    mf[I][J] = 'X';
                else
                {
                    sd_push(tree, bombs, mf, I, J, sdHT, sdWT);
                    for(i=0; i<sdHT; i++)
                        for(j=0; j<sdWT; j++)
                            tree[i][j] = ' ';
        //            b = sd_bombcount(bombs, I, J, sdHT, sdWT);
        //            if(b == 0)
        //                mf[I][J] = ' ';
        //            else
        //                mf[I][J] = '0' + b;
                }
                break;

            case 's':
                sd_print(bombs, sdHT, sdWT);
                nanosleep((struct timespec[]) {{0, 500000000}}, NULL);
                break;

            case 'Q':
                return 0;
                break;
        }
        fin = sd_finished(mf, bombs, spots, sdHT, sdWT);
    }
    t = time(NULL) - t;

    sd_print(mf, sdHT, sdWT);
    
    if(fin == 1)
        sd_win(t);
    else
        sd_lose(mf, sdHT, sdWT);

    for(i=0; i<sdHT; i++)
    {
        free(mf[i]);
        free(bombs[i]);
        free(tree[i]);
    }
    free(mf);
    free(tree);
    free(bombs);
    clear_board();
    return 0;
}

void sd_win(int t)
{
    mvprintw(5, 33, "You win!");
    mvprintw(6, 33, "Time: %d", t);
    refresh();
    getch();
}

void sd_lose(char ** mf, int sdHT, int sdWT)
{
    int burnt = 0;
    int i,j;
    int a,b,c,d;


    char ** temp = (char **) malloc(sdHT*sizeof(char *));
    for(i=0; i<sdHT; i++)
    {
        temp[i] = (char *) malloc((sdWT+1)*sizeof(char));
        for(j=0; j<sdWT; j++)
        {
            temp[i][j] = mf[i][j];
        }
        temp[i][sdWT] = '\0';
    }



    while(!burnt)
    {
        burnt = 1;
        for(i=0; i<sdHT; i++)
        {
            for(j=0; j<sdWT; j++)
            {
                if(mf[i][j] == 'X')
                    for(a=-1;a<=1; a+=2)
                        for(b=-1;b<=1; b+=2)
                        {
                            c = (a+b)/2;
                            d = (a-b)/2;
                            if(0<=i+c && i+c <  sdHT
                                       && 0   <= j+d
                                      && j+c <  sdWT)
                                temp[i+c][j+d] = 'X';
                        }
                else
                    burnt = 0;
            }
        }
        for(i=0; i<sdHT; i++)
            for(j=0; j<sdWT; j++)
                mf[i][j] = temp[i][j];
        sd_print(mf,sdHT,sdWT);
        nanosleep((struct timespec[]) {{0, 20000000}}, NULL);
    }

    for(i=0; i<sdHT; i++)
        free(temp[i]);
    free(temp);
}

char ** sd_drop_bombs(int sdHT, int sdWT, int n)
{
    int i,j,I,J;
    char ** bombs = (char **) malloc(sdHT*sizeof(char *));
    for(i=0; i<sdHT; i++)
    {
        bombs[i] = (char *) malloc((sdWT+1)*sizeof(char));
        for(j=0; j<sdWT; j++)
        {
            bombs[i][j] = ' ';
        }
        bombs[i][sdWT] = '\0';
    }

    int d = n;
    while(d!=0)
    {
        I = rand()%sdHT;
        J = rand()%sdWT;
        if(bombs[I][J] != 'X')
        {
            bombs[I][J] = 'X';
            d--;
        }
    }
    return bombs;
}

int sd_finished(char ** mf, char ** bombs, int spots, int sdHT, int sdWT)
{
    /*
     * return 0 : not finished
     * return 1 ; game won
     * return 2 : game lost
    */
    int i,j;
    int c=0;
    for(i=0; i<sdHT; i++)
        for(j=0; j<sdWT; j++)
            if(mf[i][j] == 'X')
                return 2;
    for(i=0; i<sdHT; i++)
        for(j=0; j<sdWT; j++)
            if(mf[i][j] == '.' || mf[i][j] == 'F')
                c++;
    if(c==spots)
        return 1;
    else
        return 0;
}

int sd_bombcount(char ** bombs, int I, int J, int sdHT, int sdWT)
{
    int i,j;
    int b = 0;
    for(i=-1; i<2; i++)
        for(j=-1; j<2; j++)
            if(!(i==0 && j==0) && 0<= I+i && I+i<sdHT
                               && 0<= J+j && J+j<sdWT)
                if(bombs[I+i][J+j] == 'X')
                    b++;
    return b;
}

int sd_print(char ** mf, int sdHT, int sdWT)
{
    int i,j,f;
    int in_H = (24 - sdHT)/2;
    int in_W = (80 - sdWT)/2;

    clear_board();

    for(i=0; i<sdHT; i++)
    {
        for(j=0; j<sdWT; j++)
        {
            switch(mf[i][j])
            {
                case '.':
                    attron(COLOR_PAIR(30));
                    mvprintw(in_H+i, in_W+j, "%c", mf[i][j]);
                    attroff(COLOR_PAIR(30));
                    break;

                case 'F':
                    attron(COLOR_PAIR(31));
                    mvprintw(in_H+i, in_W+j, "%c", mf[i][j]);
                    attroff(COLOR_PAIR(31));
                    break;

                case 'X':
                    if(rand()%4)
                        f = 31;
                    else
                        f = 33;
                    attron(COLOR_PAIR(f));
                    mvprintw(in_H+i, in_W+j, "%c", ' ');
                    attroff(COLOR_PAIR(f));
                    break;

                default:
                    attron(COLOR_PAIR(32));
                    mvprintw(in_H+i, in_W+j, "%c", mf[i][j]);
                    attroff(COLOR_PAIR(32));
            }
        }
    }
    refresh();
}

void sd_push(char ** tree, char ** bombs, char ** mf, int I, int J, int sdHT, int sdWT)
{
    int i,j,b;
    b = sd_bombcount(bombs, I, J, sdHT, sdWT);
    if(b == 0)
        mf[I][J] = ' ';
    else
    {
        mf[I][J] = '0' + b;
        return;
    }
    for(i=-1; i<=1; i++)
    for(j=-1; j<=1; j++)
    {
        if(!(i==0&&j==0) && 0<=i+I
                         && i+I<sdHT
                         && 0<=j+J
                         && j+J<sdWT
                         && mf[I+i][J+j] == '.'
                         && bombs[I+i][J+j] != 'X'
                         && tree[I+i][J+j] != 'X')
        {
            tree[I+i][J+j] = 'X';
            sd_push(tree, bombs, mf, I+i, J+j, sdHT, sdWT);
        }
    }
//    for(j=-1; j<=1; j+=2)
//    {
//        if(0<=j+J && j+J<sdWT
//                  && mf[I][J+j] == '.'
//                  && bombs[I][J+j] != 'X'
//                  && tree[I][J+j] != 'X')
//        {
//            tree[I][J+j] = 'X';
//            sd_push(tree, bombs, mf, I, J+j, sdHT, sdWT);
//        }
//    }
}

void sudoku()
{
    int i,j;
    int I=4;
    int J=4;
    int fin = 0;
    int lock = 0;
    char ** sud_board = init_sud_board(30);
    clear_board();
    char c;
    sud_draw(sud_board,0,0,0);
    while(!fin)
    {
        move(3+2*I,31+2*J);
        c = getch();
        switch(c)
        {
            case 'h':
                if(J>0)
                    J--;
                break;
            case 'l':
                if(J<8)
                    J++;
                break;
            case 'j':
                if(I<8)
                    I++;
                break;
            case 'k':
                if(I>0)
                    I--;
                break;
            case 'y':
                if(I>0 && J>0)
                {
                    I--;
                    J--;
                }
                break;
            case 'u':
                if(I>0 && J<8)
                {
                    I--;
                    J++;
                }
                break;
            case 'b':
                if(I<8 && J>0)
                {
                    I++;
                    J--;
                }
                break;
            case 'n':
                if(I<8 && J<8)
                {
                    I++;
                    J++;
                }
                break;
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                if(!lock)
                    lock = sud_draw(sud_board,I,J,c);
                break;
            case '0':
                lock = sud_draw(sud_board,I,J,'0');
        }
    }
}

int sud_draw(char ** sud_board, int I, int J, char c)
{
    int i,j,k,smthwr,ii,jj;
    int err=0;
    clear_board();

    for(i=0;i<19;i++)
        for(j=0;j<19;j++)
        {
            if( i%6 && j%6)
                attron(COLOR_PAIR(44));
            else
                attron(COLOR_PAIR(43));
            switch((i%2)+(j%2)*2)
            {
                case 0:
                    mvprintw(2+i,30+j,"%c",'+');
                    break;
                case 1:
                    mvprintw(2+i,30+j,"%c",'|');
                    break;
                case 2:
                    mvprintw(2+i,30+j,"%c",'-');
                    break;
            }
            if( i%6 && j%6)
                attroff(COLOR_PAIR(44));
            else
                attroff(COLOR_PAIR(43));
        }

    if(c)
    {
        sud_board[I][J] = c;
        if(c == '0')
            sud_board[I][J] = ' ';
    }

    for(i=0; i<9; i++)
        for(j=0; j<9; j++)
            if(sud_board[i][j] != ' ')
            {
                if(!sud_check(sud_board, i, j, sud_board[i][j]))
                {
                    sud_board[i][j] |= 64;
                    err=1;
                }
                else
                {
                    sud_board[i][j] &= 63;
                }
            }
                
            
    for(i=0; i<9; i++)
        for(j=0; j<9; j++)
            if(sud_board[i][j] & 64)
            {
                attron(COLOR_PAIR(42));
                mvprintw(3+2*i, 31+2*j, "%c", sud_board[i][j]&63);
                attroff(COLOR_PAIR(42));
            }
            else
            {
                attron(COLOR_PAIR(41));
                mvprintw(3+2*i, 31+2*j, "%c", sud_board[i][j]&63);
                attroff(COLOR_PAIR(41));
            }

    return err;
}

char ** init_sud_board(int diff)
{
    int i,j,k,ii,jj;
    char r;
    int d;
    int doit;

    char ** ret = (char**) malloc(10*sizeof(char*));
    for(i=0; i<9; i++)
    {
        ret[i] = (char*) malloc(10*sizeof(char));

        for(j=0; j<9; j++)
            ret[i][j] = ' ';

        ret[i][9] = '\0';
    }
    ret[9] = NULL;

    d = 0;
    while(d<diff)
    {
        doit = 1;
        i = rand()%9;
        j = rand()%9;
        r = '1' + rand()%9;
        if(ret[i][j] == ' ' && sud_check(ret, i, j, r))
        {
            ret[i][j] = r;
            d++;
        }
    }

    return ret;
}

int sud_check(char ** sud_board, int i, int j, char c)
{
    int ii, jj, k;
    for(k=0; k<9; k++)
    {
        ii = 3*(i/3)+(k%3);
        jj = 3*(j/3)+(k/3);
        if( ((c&63) == (sud_board[i][k]&63) && j!=k)
         || ((c&63) == (sud_board[k][j]&63) && i!=k)
         || ((c&63) == (sud_board[ii][jj]&63)
                && ( ii!=i || jj!=j ) ) )
            return 0;
    }
    return 1;
}


/*******************************************************************************
 * Solitaire
 ******************************************************************************/

void solitaire()
{
    clear_board();
    card * D = init_deck();

    int i,j,k;
    int I = 1;
    int J = 0;
    char c;

    sol_game * game = (sol_game*) malloc(sizeof(sol_game));
    for(i=0; i<14; i++)
        for(j=0; j<7; j++)
            game->col[i][j] = NULL;
    game->stackc = NULL;
    game->stacko = NULL;
    for(i=0; i<4; i++)
        game->base[i] = NULL;
    game->selected[0] = -1;
    game->selected[1] = -1;

    for(j=0; j<7; j++)
    {
        for(k=j+1; k<7 ;k++)
            push_card(pop_card(&D), &(game->col[0][k]));
        push_card(pop_card(&D), &(game->col[1][j]));
    }


    game->stackc = D;

    while(!sol_game_won(game))
    {
        print_sol_game(game);
        move(4+I, 26+4*J);
        c = getch();
        switch(c)
        {
            case 'h':
                if(J>0)
                    J--;
                break;
            case 'l':
                if(J<6)
                    J++;
                break;
            case 'j':
                if(I<13)
                    I++;
                break;
            case 'k':
                if(I>1)
                    I--;
                break;
            case 'y':
                if(I>1 && J>0)
                {
                    I--;
                    J--;
                }
                break;
            case 'u':
                if(I>1 && J<6)
                {
                    I--;
                    J++;
                }
                break;
            case 'b':
                if(I<13 && J>0)
                {
                    I++;
                    J--;
                }
                break;
            case 'n':
                if(I<13 && J<6)
                {
                    I++;
                    J++;
                }
                break;
            case 'd':
                draw_card(game);
                break;
            case 's':
                if(game->col[I][J] != NULL)
                {
                    game->selected[0] = I;
                    game->selected[1] = J;
                }
                break;
            case 'S':
                game->selected[0] = -1;
                game->selected[1] = -1;
                break;
            case 'm':
                move_cards(game, I, J);
                break;
            case 't':
                take_card(game, I, J);
                break;
            case '1':
            case '2':
            case '3':
            case '4':
                base_change(game, I, J, c-'1');
                break;
            case 'f':
                flip_card(game, J);
                break;
            case 'a':
                take_to_base(game);
                break;
            case '?':
                sol_show_help();
                getch();
                break;
        }
    }

    print_sol_game(game);
    free_sol_game(game);

    win_sol_game();
    clear_board();
}


void free_sol_game(sol_game * game)
{
    int i,j;

    for(i=0; i<4; i++)
        free_deck(game->base[i]);
    
    for(i=0; i<14; i++)
        for(j=0; j<7; j++)
            free_deck(game->col[i][j]);
    
    free_deck(game->stacko);
    free_deck(game->stackc);

    free(game);
}

void free_deck(card * D)
{
    card *C1, *C2;
    C1 = D;
    while(C1 != NULL)
    {
        C2 = C1->next;
        free(C1);
        C1 = C2;
    }
    
}

void sol_show_help()
{
    attron(COLOR_PAIR(90));
    mvprintw(4 , 27, "          Keys:           ");
    mvprintw(5 , 27, " hljkyubn:...........move ");
    mvprintw(6 , 27, " s:...........select card ");
    mvprintw(7 , 27, " S:.........unselect card ");
    mvprintw(8 , 27, " m:.............move card ");
    mvprintw(9 , 27, " f:.............flip card ");
    mvprintw(10, 27, " d:.............draw card ");
    mvprintw(11, 27, " t:.............take card ");
    mvprintw(12, 27, " a,1-4:......move to base ");
    mvprintw(13, 27, " ?:..................help ");
    mvprintw(14, 27, " Press any key to return! ");
    attroff(COLOR_PAIR(90));
}

void win_sol_game()
{
    int i,j,I,J;

    int c;
    int h;
    int r;

    char * S = "HCDS";
    card * C = (card*) malloc(sizeof(card));

    for(i=0; i<40; i++)
    {
        c = rand()%80;
        h = rand()%20 + 10;
        r = rand()%80 - 40;
        for(j=-60; j<60; j++)
        {
            C->n = rand()%13 + 1;
            C->s = S[rand()%4];
            I = 23-(h*(3600-j*j))/3600;
            J = c + r*j/60;
            if(I<24 && J<79)
            {
                print_card(I, J, C, rand()%2);
                refresh();
            }
            nanosleep((struct timespec[]){{0, 3000000}}, NULL);
        }
    }

}

void take_to_base(sol_game * game)
{
    int i;
    int dummy;

    if(game->stacko == NULL)
        return;

    for(i=0; i<4; i++)
    {
        if(game->base[i] == NULL && game->stacko->n == 1)
        {
            push_card(pop_card(&(game->stacko)), &(game->base[i]));
            return;
        }
        if(game->base[i] != NULL
            && game->stacko->n == game->base[i]->n+1
            && game->stacko->s == game->base[i]->s)
        {
            push_card(pop_card(&(game->stacko)), &(game->base[i]));
            return;
        }
    }
}

void flip_card(sol_game * game, int J)
{
    if(game->col[1][J] != NULL || game->col[0][J] == NULL)
        return;
    push_card(pop_card(&(game->col[0][J])), &(game->col[1][J]));
}

int card_fits(sol_game * game, card * C, int I, int J)
{
    if(C == NULL)
        return 0;

    if(game->col[I][J] != NULL)
        return 0;
    
    if(I == 1)
        return 1;
    
    if(game->col[I-1][J] == NULL)
        return 0;
    
    if(C->n != game->col[I-1][J]->n - 1
        || (C->s + game->col[I-1][J]->s)%2 != 1)
        return 0;

    return 1;
}

void base_change(sol_game * game, int I, int J, int b)
{
    if(game->col[I][J] != NULL
        && game->col[I][J]->n == 1
        && game->base[b] == NULL)
    {
        push_card(pop_card(&(game->col[I][J])), &(game->base[b]));
        return;
    }

    if(game->col[I][J] != NULL
        && game->base[b] != NULL
        && game->col[I][J]->s == game->base[b]->s
        && game->col[I][J]->n == game->base[b]->n+1)
    {
        push_card(pop_card(&(game->col[I][J])), &(game->base[b]));
        return;
    }

    if(game->base[b] != NULL
        && card_fits(game, game->base[b], I, J))
    {
        push_card(pop_card(&(game->base[b])), &(game->col[I][J]));
        return;
    }
}

void take_card(sol_game * game, int I, int J)
{
    if(game->stacko == NULL)
        return;
    if(card_fits(game, game->stacko, I, J))
        push_card(pop_card(&(game->stacko)), &(game->col[I][J]));
}

void move_cards(sol_game * game, int I, int J)
{
    int i = game->selected[0];
    int j = game->selected[1];
    if(i == -1 || j == -1)
        return;

    if(!card_fits(game, game->col[i][j], I, J))
        return;

    int k = 0;
    while(i+k < 14 && game->col[i+k][j] != NULL)
    {
        push_card(pop_card(&(game->col[i+k][j])), &(game->col[I+k][J]));
        k++;
    }
    game->selected[0] = -1;
    game->selected[1] = -1;
}

int sol_game_won(sol_game * game)
{
    int i;
    for(i=0; i<4; i++)
        if(deck_length(game->base[i]) < 13)
            return 0;
    return 1;
}

void draw_card(sol_game * game)
{
    if(game->stackc == NULL)
    {
        flip_deck(&(game->stacko));
        game->stackc = game->stacko;
        game->stacko = NULL;
    }
    else
        push_card(pop_card(&(game->stackc)), &(game->stacko));
}

void flip_deck(card ** DD)
{
    if(*DD != NULL && (*DD)->prev != NULL)
        mvprintw(23,0, "Warning: flipping a nontop!");
    card * C = *DD;
    card * Cr = NULL;
    card * dummy;

    while(C != NULL)
    {
        dummy = C->next;
        C->next = C->prev;
        C->prev = dummy;
        Cr = C;
        C = C->prev;
    }

    *DD = Cr;
}

void print_sol_game(sol_game * game)
{
    int i,j;


    attron(COLOR_PAIR(53));
    for(i=0; i<18; i++)
        mvprintw(1+i, 26, "                            ");
    attroff(COLOR_PAIR(53));

    print_card(2, 27, game->stackc, 1);
    print_card(2, 31, game->stacko, 0);

    for(i=0; i<4; i++)
        print_card(2, 39+4*i, game->base[i], 0);

    for(j=0; j<7; j++)
    {
        print_card(4, 27+4*j, game->col[0][j], 1);
        for(i=1; i<14; i++)
            print_card(4+i, 27+4*j, game->col[i][j], 0);
    }
    if(game->selected[0] != -1)
    {
        attron(COLOR_PAIR(53));
        mvprintw(4+game->selected[0],26+4*game->selected[1],"*");
        attroff(COLOR_PAIR(53));
    }
    refresh();
}

card * pop_card(card ** DD)
{
    if(*DD == NULL)
        return NULL;
    
    if((*DD)->prev != NULL)
        mvprintw(23,0, "Warning: popping a nontop!");

    card * C = *DD;
    *DD = C->next;
    if(*DD != NULL)
        (*DD)->prev = NULL;

    return C;
}

void push_card(card * C, card ** DD)
{
    if(C == NULL)
        return;
    
    if(*DD != NULL && (*DD)->prev != NULL)
        mvprintw(23,0,"Warning: pushing onto nontop!");

    C->next = *DD;
    C->prev = NULL;

    if(*DD != NULL)
        (*DD)->prev = C;

    *DD = C;
}

void print_card(int i, int j, card * C, int hidden)
{
    if(C == NULL)
    {
        attron(COLOR_PAIR(53));
        mvprintw(i,j,"  ");
        attroff(COLOR_PAIR(53));
        return;
    }

    if(hidden)
    {
        attron(COLOR_PAIR(50));
        mvprintw(i,j,"??");
        attroff(COLOR_PAIR(50));
        return;
    }

    char cn;
    switch(C->n)
    {
        case 10:
            cn = 'T';
            break;
        case 11:
            cn = 'J';
            break;
        case 12:
            cn = 'Q';
            break;
        case 13:
            cn = 'K';
            break;
        case 1:
            cn = 'A';
            break;
        default:
            cn = '2' + C->n - 2;
    }
    attron(COLOR_PAIR(51+(C->s%2)));
    mvprintw(i,j,"%c%c", cn, C->s);
    attroff(COLOR_PAIR(51+(C->s%2)));
}

card * init_deck()
{
    int i,j;
    char * S = "HCDS";
    card * D = NULL;
    card * dummy;

    for(i=1; i<14; i++)
        for(j=0; j<4; j++)
        {
            dummy = (card*) malloc(sizeof(card));
            dummy->n = i;
            dummy->s = S[j];
            dummy->next = NULL;
            dummy->prev = NULL;
            push_card(dummy, &D);
        }

    shuffle_deck(&D);
    
    return D;
}

int deck_length(card * C)
{
    int l = 0;
    card * D = C;
    while(D != NULL)
    {
        D = D->next;
        l++;
    }
    return l;
}

void shuffle_deck(card ** CC)
{
    int i,j,r;
    card * a[2];
    card * D = *CC;

    int l = deck_length(D);
    if(l<2)
        return;

    for(j=0; j<7; j++)
    {
        a[0] = D;
        a[1] = D;
        for(i=0; i<l/2; i++)
            a[1] = a[1]->next;
        a[1]->prev->next = NULL;
        a[1]->prev = NULL;

        D = NULL;

        while(a[0] != NULL || a[1] != NULL)
        {
            if(a[0] == NULL)
            {
                push_card(pop_card(&(a[1])), &D);
                continue;
            }
            if(a[1] == NULL)
            {
                push_card(pop_card(&(a[0])), &D);
                continue;
            }

            r = rand()%2;
            push_card(pop_card(&(a[r])), &D);
        }
    }

    *CC = D;
}

/*******************************************************************************
 * betris
 ******************************************************************************/

void betris()
{
    int i,j,k;
    int score = 0;
    int lose = 0;
    char c;
    clear_board();
    refresh();

    char ** bet_board = malloc(21*sizeof(char*));
    for(i=0; i<20; i++)
    {
        bet_board[i] = malloc(11*sizeof(char));
        strcpy(bet_board[i], "          ");
    }

    char **** bet_model = bet_init_models();

    bet_piece piece, next_piece, dummy;

    piece.type = rand()%7;
    piece.direction = 0;
    piece.y = 0;
    piece.x = 3;

    next_piece.type = rand()%7;
    next_piece.direction = 0;
    next_piece.y = 0;
    next_piece.x = 3;

    int FD[2];
    pipe(FD);

    pid_t tictoc = fork();
    if(tictoc == 0)
    {
        FILE * stream = fdopen(FD[1], "w");
        while(1)
        {
            sleep(1);
            write(FD[1], "t", sizeof(char));
        }
    }

    pid_t inp = fork();
    if(inp == 0)
    {
        FILE * stream = fdopen(FD[1], "w");
        while(1)
        {
            c = getch();
            write(FD[1], &c, sizeof(char));
        }
    }

    FILE * stream = fdopen(FD[0], "r");

    while(!lose)
    {
        clear_board();
        betris_print(bet_board, piece);
        bet_print_piece(next_piece,bet_model,1);
        bet_print_piece(piece,bet_model,0);
        mvprintw(5,5,"Score: %i", score);

        move(21,46);
        refresh();
        c = fgetc(stream);
        switch(c)
        {
            case 't':
                dummy = piece;
                dummy.y++;
                if(betris_fits(bet_board, dummy, bet_model))
                {
                    piece.y++;
                }
                else
                {
                    if(!betris_fits(bet_board, piece, bet_model))
                        lose = 1;
                    else
                    {
                        bet_write_piece(bet_board, piece, bet_model);
                        piece = next_piece;
                        next_piece.type = rand()%7;
                        next_piece.direction = 0;
                        next_piece.y = 0;
                        next_piece.x = 3;
                    }
                }
                break;

            case 'h':
                dummy = piece;
                dummy.x--;
                if(betris_fits(bet_board, dummy, bet_model))
                    piece.x--;
                break;

            case 'l':
                dummy = piece;
                dummy.x++;
                if(betris_fits(bet_board, dummy, bet_model))
                    piece.x++;
                break;

            case ' ':
                dummy = piece;
                while(betris_fits(bet_board, dummy, bet_model))
                {
                    piece = dummy;
                    dummy.y++;
                }
                break;

            case 'k':
                dummy = piece;
                dummy.direction = (dummy.direction+1)%4;
                if(betris_fits(bet_board, dummy, bet_model))
                    piece.direction = (piece.direction+1)%4;
                break;
            
            case 'q':
                lose = 1;
                break;
        }
        score += betris_rollup(bet_board);
    }
    kill(tictoc,SIGKILL);
    kill(inp,SIGKILL);

    clear_board();
}

int betris_rollup(char ** bet_board)
{
    int i,j,k,go;
    int r = 0;
    for(i=0; i<20; i++)
    {
        go = 1;
        for(j=0; j<10; j++)
            if(bet_board[i][j] == ' ')
                go = 0;
        if(go)
        {
            r++;
            for(k=i-1; k>=0; k--)
                strcpy(bet_board[k+1], bet_board[k]);
            strcpy(bet_board[0], "          ");
        }
    }
    return r*(r+1)/2;
}

int betris_fits(char ** bet_board, bet_piece piece, char **** bet_model)
{
    int i,j;
    if(piece.y>=20)
        return 0;
    for(i=0; i<4 && piece.y-i>=0; i++)
        for(j=0; j<4; j++)
            if(bet_model[piece.type][piece.direction][i][j] == 'X'
                && 0 <= piece.y-i < 20
                && 0 <= piece.x+j < 10
                && bet_board[piece.y-i][piece.x+j] != ' ')
                return 0;
    for(i=0; i<4; i++)
        for(j=0; j<4; j++)
            if(bet_model[piece.type][piece.direction][i][j] == 'X' &&
                (      20 <= piece.y-i
                    || 0  >  piece.x+j
                    || piece.x+j >= 10))
                return 0;
    return 1;
}

char **** bet_init_models()
{
    int i,j,k;
    char **** bet_model = malloc(8*sizeof(char***));
    for(i=0; i<7; i++)
    {
        bet_model[i] = malloc(5*sizeof(char**));
        for(j=0; j<4; j++)
        {
            bet_model[i][j] = malloc(5*sizeof(char*));
            for(k=0; k<4; k++)
                bet_model[i][j][k] = malloc(5*sizeof(char));
        }
    }

//  0 = I
    strcpy(bet_model[0][0][3], "    ");
    strcpy(bet_model[0][0][2], "    ");
    strcpy(bet_model[0][0][1], "    ");
    strcpy(bet_model[0][0][0], "XXXX");

    strcpy(bet_model[0][1][3], " X  ");
    strcpy(bet_model[0][1][2], " X  ");
    strcpy(bet_model[0][1][1], " X  ");
    strcpy(bet_model[0][1][0], " X  ");

    strcpy(bet_model[0][2][3], "    ");
    strcpy(bet_model[0][2][2], "    ");
    strcpy(bet_model[0][2][1], "    ");
    strcpy(bet_model[0][2][0], "XXXX");

    strcpy(bet_model[0][3][3], " X  ");
    strcpy(bet_model[0][3][2], " X  ");
    strcpy(bet_model[0][3][1], " X  ");
    strcpy(bet_model[0][3][0], " X  ");

//  1 = J
    strcpy(bet_model[1][0][3], "    ");
    strcpy(bet_model[1][0][2], "  X ");
    strcpy(bet_model[1][0][1], "  X ");
    strcpy(bet_model[1][0][0], " XX ");

    strcpy(bet_model[1][1][3], "    ");
    strcpy(bet_model[1][1][2], "    ");
    strcpy(bet_model[1][1][1], "XXX ");
    strcpy(bet_model[1][1][0], "  X ");

    strcpy(bet_model[1][2][3], "    ");
    strcpy(bet_model[1][2][2], " XX ");
    strcpy(bet_model[1][2][1], " X  ");
    strcpy(bet_model[1][2][0], " X  ");

    strcpy(bet_model[1][3][3], "    ");
    strcpy(bet_model[1][3][2], "    ");
    strcpy(bet_model[1][3][1], " X  ");
    strcpy(bet_model[1][3][0], " XXX");

//  2 = L
    strcpy(bet_model[2][0][3], "    ");
    strcpy(bet_model[2][0][2], " X  ");
    strcpy(bet_model[2][0][1], " X  ");
    strcpy(bet_model[2][0][0], " XX ");

    strcpy(bet_model[2][1][3], "    ");
    strcpy(bet_model[2][1][2], "    ");
    strcpy(bet_model[2][1][1], "  X ");
    strcpy(bet_model[2][1][0], "XXX ");

    strcpy(bet_model[2][2][3], "    ");
    strcpy(bet_model[2][2][2], " XX ");
    strcpy(bet_model[2][2][1], "  X ");
    strcpy(bet_model[2][2][0], "  X ");

    strcpy(bet_model[2][3][3], "    ");
    strcpy(bet_model[2][3][2], "    ");
    strcpy(bet_model[2][3][1], " XXX");
    strcpy(bet_model[2][3][0], " X  ");

//  3 = O
    strcpy(bet_model[3][0][3], "    ");
    strcpy(bet_model[3][0][2], "    ");
    strcpy(bet_model[3][0][1], " XX ");
    strcpy(bet_model[3][0][0], " XX ");

    strcpy(bet_model[3][1][3], "    ");
    strcpy(bet_model[3][1][2], "    ");
    strcpy(bet_model[3][1][1], " XX ");
    strcpy(bet_model[3][1][0], " XX ");

    strcpy(bet_model[3][2][3], "    ");
    strcpy(bet_model[3][2][2], "    ");
    strcpy(bet_model[3][2][1], " XX ");
    strcpy(bet_model[3][2][0], " XX ");

    strcpy(bet_model[3][3][3], "    ");
    strcpy(bet_model[3][3][2], "    ");
    strcpy(bet_model[3][3][1], " XX ");
    strcpy(bet_model[3][3][0], " XX ");

//  4 = S
    strcpy(bet_model[4][0][3], "    ");
    strcpy(bet_model[4][0][2], "    ");
    strcpy(bet_model[4][0][1], "  XX");
    strcpy(bet_model[4][0][0], " XX ");

    strcpy(bet_model[4][1][3], "    ");
    strcpy(bet_model[4][1][2], " X  ");
    strcpy(bet_model[4][1][1], " XX ");
    strcpy(bet_model[4][1][0], "  X ");

    strcpy(bet_model[4][2][3], "    ");
    strcpy(bet_model[4][2][2], "    ");
    strcpy(bet_model[4][2][1], "  XX");
    strcpy(bet_model[4][2][0], " XX ");

    strcpy(bet_model[4][3][3], "    ");
    strcpy(bet_model[4][3][2], " X  ");
    strcpy(bet_model[4][3][1], " XX ");
    strcpy(bet_model[4][3][0], "  X ");

//  5 = T
    strcpy(bet_model[5][0][3], "    ");
    strcpy(bet_model[5][0][2], "    ");
    strcpy(bet_model[5][0][1], "XXX ");
    strcpy(bet_model[5][0][0], " X  ");

    strcpy(bet_model[5][1][3], "    ");
    strcpy(bet_model[5][1][2], " X  ");
    strcpy(bet_model[5][1][1], " XX ");
    strcpy(bet_model[5][1][0], " X  ");

    strcpy(bet_model[5][2][3], "    ");
    strcpy(bet_model[5][2][2], "    ");
    strcpy(bet_model[5][2][1], " X  ");
    strcpy(bet_model[5][2][0], "XXX ");

    strcpy(bet_model[5][3][3], "    ");
    strcpy(bet_model[5][3][2], " X  ");
    strcpy(bet_model[5][3][1], "XX  ");
    strcpy(bet_model[5][3][0], " X  ");

//  6 = Z
    strcpy(bet_model[6][0][3], "    ");
    strcpy(bet_model[6][0][2], "    ");
    strcpy(bet_model[6][0][1], "XX  ");
    strcpy(bet_model[6][0][0], " XX ");

    strcpy(bet_model[6][1][3], "    ");
    strcpy(bet_model[6][1][2], "  X ");
    strcpy(bet_model[6][1][1], " XX ");
    strcpy(bet_model[6][1][0], " X  ");

    strcpy(bet_model[6][2][3], "    ");
    strcpy(bet_model[6][2][2], "    ");
    strcpy(bet_model[6][2][1], "XX  ");
    strcpy(bet_model[6][2][0], " XX ");

    strcpy(bet_model[6][3][3], "    ");
    strcpy(bet_model[6][3][2], "  X ");
    strcpy(bet_model[6][3][1], " XX ");
    strcpy(bet_model[6][3][0], " X  ");
    
    return bet_model;
}

void betris_print(char ** B, bet_piece piece)
{
    int i,j;
    mvprintw( 0,34, "+----------+");
    mvprintw(21,34, "+----------+");
    for(i=0; i<20; i++)
    {
        for(j=0; j<10; j++)
        {
            mvprintw(1+i, 34, "|");
            mvprintw(1+i, 45, "|");
            attron(COLOR_PAIR(60 + (int)(B[i][j] - ' ')));
            mvprintw(1+i, 35+j, ".");
            attroff(COLOR_PAIR(60 + (int)(B[i][j] - ' ')));
        }
    }

    mvprintw(1,47, "+----+");
    for(i=0; i<4; i++)
    {
        mvprintw(i+2, 47, "|");
        mvprintw(i+2, 52, "|");
    }
    mvprintw(6,47, "+----+");

}

void bet_print_piece(bet_piece piece, char **** models, int next)
{
    int i,j;
    if(next == 0)
        for(i=0; i<4 && piece.y - i >= 0; i++)
            for(j=0; j<4; j++)
                if(models[piece.type][piece.direction][i][j] == 'X')
                {
                    attron(COLOR_PAIR(60 + piece.type+1));
                    mvprintw(piece.y + 1-i, piece.x + 35+j, " ");
                    attroff(COLOR_PAIR(60 + piece.type+1));
                }
    if(next == 1)
        for(i=0; i<4; i++)
            for(j=0; j<4; j++)
                if(models[piece.type][piece.direction][i][j] == 'X')
                {
                    attron(COLOR_PAIR(60 + piece.type+1));
                    mvprintw(5-i, 48+j, " ");
                    attroff(COLOR_PAIR(60 + piece.type+1));
                }
                else
                {
                    attron(COLOR_PAIR(60));
                    mvprintw(5-i, 48+j, " ");
                    attroff(COLOR_PAIR(60));
                }
}

void bet_write_piece(char ** bet_board, bet_piece piece, char **** models)
{
    int i,j;
    for(i=0; i<4; i++)
        for(j=0; j<4; j++)
            if(models[piece.type][piece.direction][i][j] == 'X'
                && 0 <= piece.x < 10
                && 0 <= piece.y-i < 20)
                bet_board[piece.y-i][piece.x+j] = ' ' + 1 + piece.type;
}
